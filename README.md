# gitrics

[![PyPI version](https://badge.fury.io/py/gitrics.svg)](https://pypi.org/project/gitrics/)

Python package to generate metrics based off git usage.
